﻿SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
#SingleInstance force ; Only run one copy of this at once

; OSP Easy UI - Tony Slagle and Mike Grose
; 
; Copyright (c) 2015, Okuma America Corporation
; 
; Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
; 
; If you did not receive source code with this software, you may obtain a copy from http://github.com/
; 
; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.




;Need to make a tray icon to meet app store requirements
;#NoTrayIcon

; NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX
; Need to make tray icon have a menu item when right click to show an "About" box with %about_text% and %isc_license_text%

;Reusable text for the splash screen and about box.
about_text=
(
SimToolBox
Quit SimToolBox -- Ctrl+Alt+Shift+Q
Right Click -- Ctrl+Shift+Click
Toggle Topmost -- Ctrl+Shift+T
Pull Window Back To Screen -- Ctrl+Shift+F
Get Public IP address -- Ctrl+Shift+P
Get Local IP address -- Ctrl+Shift+L
)

;This software is released under ISC license
isc_license_text=
(
Copyright (c) 2015, Okuma America Corporation

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

If you did not receive source code with this software, you may obtain a copy from http://github.com/

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
)

;Show the about_text string in a splash screen popup when the program starts
SplashTextOn,325,100,"SimToolBox",%about_text%
Sleep, 4000
SplashTextOff
return

; Should do goodbye splash screen when exiting
^!+Q:: ExitApp

^+T::
  Winset, Alwaysontop, , A
  SplashTextOn,300,100,%SplashTextTitle%,
  (
    %A% Toggled TopMost
  )
  Sleep, 1500
  SplashTextOff
return

^+LButton::RButton
return

; NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX NEED FIX
; Check window we're about to move and make sure its parent is NOT EBI-FRY . (don't want to move OSP windows)
; Can use WindowSpy (comes with AutoHotKey install) to get window names and parent name

;Move windows that are off the left side of the screen or under the black top status bar
^+F::
  ;need to make sure active window is not an OSP window. if it is, return
  WinGetPos,X,Y,,,A
  if ( X < 0 )
  {
    WinMove,A,,0,
  }
  if ( Y < 112)
  {
    WinMove,A,,,112
  }
  SplashTextOn,300,100,%SplashTextTitle%,
  (
    %A% moved to viewable area
    OldPos=%X%,%Y%
  )
  Sleep, 1500
  SplashTextOff
return

; Gives a confusing error if not connected to network...
^+P::
MsgBox % GetPublicIP() ;Display the networks public IP
Return

; Gives a confusing error if not connected to network...
^+L::
MsgBox % GetLocalIPByAdaptor("Ethernet") ;Display the local IP of the adaptor named "Ethernet"
For AdaptorName, IP in GetLocalIPs() { ;Display the IPs of all adaptors that have one
    MsgBox, %AdaptorName%: %IP%
}
Return

;Function used by GetPublicIP and GetLocalIP
GetPublicIP() {
    HttpObj := ComObjCreate("WinHttp.WinHttpRequest.5.1")
    HttpObj.Open("GET","https://www.google.com/search?q=what+is+my+ip&num=1")
    HttpObj.Send()
    RegexMatch(HttpObj.ResponseText,"Client IP address: ([\d\.]+)",match)
    Return match1
}

;Function used by GetPublicIP and GetLocalIP
GetLocalIPByAdaptor(adaptorName) {
    objWMIService := ComObjGet("winmgmts:{impersonationLevel = impersonate}!\\.\root\cimv2")
    colItems := objWMIService.ExecQuery("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionID = '" adaptorName "'")._NewEnum, colItems[objItem]
    colItems := objWMIService.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE InterfaceIndex = '" objItem.InterfaceIndex "'")._NewEnum, colItems[objItem]
    Return objItem.IPAddress[0]
}

;Function used by GetPublicIP and GetLocalIP
GetLocalIPs() {
    adaptors := Object()
    ips := Object()
    objWMIService := ComObjGet("winmgmts:{impersonationLevel = impersonate}!\\.\root\cimv2")
    colItems := objWMIService.ExecQuery("SELECT * FROM Win32_NetworkAdapter")._NewEnum, colItems[objItem]
    While (colItems[objItem])
        adaptors.Insert(objItem.InterfaceIndex,objItem.NetConnectionID)
    For index, name in adaptors {
        colItems := objWMIService.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE InterfaceIndex = '" index "'")._NewEnum, colItems[objItem]
        If (name && objItem.IPAddress[0])
            ips.Insert(name,objItem.IPAddress[0])
    }
    Return ips
}


;; Allow move ANY window including OSP windows you shouldn't move...
;; (commented out for production version)

;Alt & LButton::
;CoordMode, Mouse  ; Switch to screen/absolute coordinates.
;MouseGetPos, EWD_MouseStartX, EWD_MouseStartY, EWD_MouseWin
;WinGetPos, EWD_OriginalPosX, EWD_OriginalPosY,,, ahk_id %EWD_MouseWin%
;WinGet, EWD_WinState, MinMax, ahk_id %EWD_MouseWin% 
;if EWD_WinState = 0  ; Only if the window isn't maximized 
;    SetTimer, EWD_WatchMouse, 10 ; Track the mouse as the user drags it.
;return
;
;EWD_WatchMouse:
;GetKeyState, EWD_LButtonState, LButton, P
;if EWD_LButtonState = U  ; Button has been released, so drag is complete.
;{
;    SetTimer, EWD_WatchMouse, off
;    return
;}
;GetKeyState, EWD_EscapeState, Escape, P
;if EWD_EscapeState = D  ; Escape has been pressed, so drag is cancelled.
;{
;    SetTimer, EWD_WatchMouse, off
;    WinMove, ahk_id %EWD_MouseWin%,, %EWD_OriginalPosX%, %EWD_OriginalPosY%
;    return
;}
;; Otherwise, reposition the window to match the change in mouse coordinates
;; caused by the user having dragged the mouse:
;CoordMode, Mouse
;MouseGetPos, EWD_MouseX, EWD_MouseY
;WinGetPos, EWD_WinX, EWD_WinY,,, ahk_id %EWD_MouseWin%
;SetWinDelay, -1   ; Makes the below move faster/smoother.
;WinMove, ahk_id %EWD_MouseWin%,, EWD_WinX + EWD_MouseX - EWD_MouseStartX, EWD_WinY + EWD_MouseY - EWD_MouseStartY
;EWD_MouseStartX := EWD_MouseX  ; Update for the next timer-call to this subroutine.
;EWD_MouseStartY := EWD_MouseY
;return
